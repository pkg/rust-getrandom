Source: rust-getrandom
Section: rust
Priority: optional
Build-Depends: debhelper-compat (= 13),
 dh-sequence-cargo,
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-cfg-if-1+default-dev <!nocheck>,
 librust-libc-0.2-dev (>= 0.2.154-~~) <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 kpcyrd <git@rxv.cc>
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/getrandom]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/getrandom
Homepage: https://github.com/rust-random/getrandom
X-Cargo-Crate: getrandom
Rules-Requires-Root: no

Package: librust-getrandom-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-cfg-if-1+default-dev,
 librust-libc-0.2-dev (>= 0.2.154-~~)
Suggests:
 librust-getrandom+compiler-builtins-dev (= ${binary:Version}),
 librust-getrandom+core-dev (= ${binary:Version}),
 librust-getrandom+rustc-dep-of-std-dev (= ${binary:Version})
Provides:
 librust-getrandom+custom-dev (= ${binary:Version}),
 librust-getrandom+default-dev (= ${binary:Version}),
 librust-getrandom+linux-disable-fallback-dev (= ${binary:Version}),
 librust-getrandom+rdrand-dev (= ${binary:Version}),
 librust-getrandom+std-dev (= ${binary:Version}),
 librust-getrandom+test-in-browser-dev (= ${binary:Version}),
 librust-getrandom-0-dev (= ${binary:Version}),
 librust-getrandom-0+custom-dev (= ${binary:Version}),
 librust-getrandom-0+default-dev (= ${binary:Version}),
 librust-getrandom-0+linux-disable-fallback-dev (= ${binary:Version}),
 librust-getrandom-0+rdrand-dev (= ${binary:Version}),
 librust-getrandom-0+std-dev (= ${binary:Version}),
 librust-getrandom-0+test-in-browser-dev (= ${binary:Version}),
 librust-getrandom-0.2-dev (= ${binary:Version}),
 librust-getrandom-0.2+custom-dev (= ${binary:Version}),
 librust-getrandom-0.2+default-dev (= ${binary:Version}),
 librust-getrandom-0.2+linux-disable-fallback-dev (= ${binary:Version}),
 librust-getrandom-0.2+rdrand-dev (= ${binary:Version}),
 librust-getrandom-0.2+std-dev (= ${binary:Version}),
 librust-getrandom-0.2+test-in-browser-dev (= ${binary:Version}),
 librust-getrandom-0.2.15-dev (= ${binary:Version}),
 librust-getrandom-0.2.15+custom-dev (= ${binary:Version}),
 librust-getrandom-0.2.15+default-dev (= ${binary:Version}),
 librust-getrandom-0.2.15+linux-disable-fallback-dev (= ${binary:Version}),
 librust-getrandom-0.2.15+rdrand-dev (= ${binary:Version}),
 librust-getrandom-0.2.15+std-dev (= ${binary:Version}),
 librust-getrandom-0.2.15+test-in-browser-dev (= ${binary:Version})
Description: Retrieve random data from system source - Rust source code
 Source code for Debianized Rust crate "getrandom"

Package: librust-getrandom+compiler-builtins-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-getrandom-dev (= ${binary:Version}),
 librust-compiler-builtins-0.1+default-dev
Provides:
 librust-getrandom-0+compiler-builtins-dev (= ${binary:Version}),
 librust-getrandom-0.2+compiler-builtins-dev (= ${binary:Version}),
 librust-getrandom-0.2.15+compiler-builtins-dev (= ${binary:Version})
Description: Retrieve random data from system source - feature "compiler_builtins"
 This metapackage enables feature "compiler_builtins" for the Rust getrandom
 crate, by pulling in any additional dependencies needed by that feature.

Package: librust-getrandom+core-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-getrandom-dev (= ${binary:Version}),
 librust-rustc-std-workspace-core-1+default-dev
Provides:
 librust-getrandom-0+core-dev (= ${binary:Version}),
 librust-getrandom-0.2+core-dev (= ${binary:Version}),
 librust-getrandom-0.2.15+core-dev (= ${binary:Version})
Description: Retrieve random data from system source - feature "core"
 This metapackage enables feature "core" for the Rust getrandom crate, by
 pulling in any additional dependencies needed by that feature.

Package: librust-getrandom+rustc-dep-of-std-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-getrandom-dev (= ${binary:Version}),
 librust-getrandom+compiler-builtins-dev (= ${binary:Version}),
 librust-getrandom+core-dev (= ${binary:Version}),
 librust-libc-0.2+rustc-dep-of-std-dev (>= 0.2.154-~~)
Provides:
 librust-getrandom-0+rustc-dep-of-std-dev (= ${binary:Version}),
 librust-getrandom-0.2+rustc-dep-of-std-dev (= ${binary:Version}),
 librust-getrandom-0.2.15+rustc-dep-of-std-dev (= ${binary:Version})
Description: Retrieve random data from system source - feature "rustc-dep-of-std"
 This metapackage enables feature "rustc-dep-of-std" for the Rust getrandom
 crate, by pulling in any additional dependencies needed by that feature.
